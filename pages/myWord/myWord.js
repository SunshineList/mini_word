// pages/all_word/all_word.js
var app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    offset: 0,
    hasMoreData: false,
    hasSearch: false,
    size: 20,
    search: "",
    all_words: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this
    that.getInfo()
  },

  getInputValue: function (e) {
    var that = this;
    that.setData({
      'search': e.detail.value,
      'offset': 0,
      'hasSearch': true,
      'hasMoreData': true,
      'all_words': [],
    })
    that.search_list()
  },

  /**
   * 封装设置列表对象
   * @param {} data 
   */
  setWordData: function (data) {
    var that = this;
    for (var i = 0; i < data.length; i++) {
      var newarray = {
        id: data[i].id,
        word: data[i].word,
        mean: data[i].mean,
        real_mean: data[i].real_mean,
        created_time: data[i].created_time,
        speech: data[i].speech,
      };
      that.data.all_words = that.data.all_words.concat(newarray);
      that.setData({
        all_words: that.data.all_words
      });
    }
  },
  /**
   * 搜索
   */
  search_list: function () {
    var that = this;
    wx.request({
      url: app.globalData.globalReqUrl + "/v1/api/word/custom_word/",
      header: { 'Authorization': 'Token ' + wx.getStorageSync("token") },
      data: { "search": that.data.search, "limit": 20, "offset": that.data.offset },
      method: "GET",
      success: res => {
        if (!res.data.next) {
          that.setData({
            hasMoreData: false
          })
        }
        that.setWordData(res.data.results)
      }
    })
  },

  /**
   * 单词库
   */
  getInfo: function () {
    var that = this;
    wx.showLoading({
      title: '加载中',
    }),
      wx.request({
        url: app.globalData.globalReqUrl + "/v1/api/word/custom_word/",
        header: { 'Authorization': 'Token ' + wx.getStorageSync("token") },
        data: { "limit": 20, "offset": that.data.offset },
        method: "GET",
        success: res => {
          wx.hideLoading();
          if (res.data.next) {
            that.setData({ "hasMoreData": true })
          } else {
            that.setData({ "hasMoreData": false })
          }
          that.setWordData(res.data.results)
        }
      })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    // this.search_list();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.data.offset = 0;
    this.getInfo()
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    if (this.data.hasSearch && this.data.hasMoreData) {
      this.data.offset = this.data.offset + this.data.size;
      this.search_list()
    } else if (this.data.hasMoreData) {
      this.data.offset = this.data.offset + 20;
      this.getInfo()
    } else {
      wx.showToast({
        title: '没有更多数据了',
      })
    }
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})