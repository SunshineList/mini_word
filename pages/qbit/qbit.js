// pages/qbit/qbit.js
var app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    username: "",
    password: "",
    host: "https://hmlyq.outputbug.com:8889"
  },

  changeUsername(e) {
    this.setData({
      "username": e.detail.value
    })
  },
  changePassword(e) {
    this.setData({
      "password": e.detail.value
    })
  },
  changeHost(e) {
    this.setData({
      "host": e.detail.value
    })
  },

  btnClick(e) {
    if (!this.data.username || !this.data.password || !this.data.host) {
      wx.showToast({
        title: '请完善必填信息',
        icon: 'error',
        duration: 2000
      })
    } else {
      wx.request({
        url: app.globalData.globaQbitUrl + "/login",
        method: "post",
        data: { 'username': this.data.username, 'password': this.data.password, 'host': this.data.host },
        success: res => {
          console.log(res)
          if (res.data.code != 200) {
            wx.showToast({
              title: res.data.message,
              icon: 'error',
              duration: 2000
            })
          } else {
            wx.setStorageSync('qbittoken', 200)
            app.jumpToQbitPage('/pages/qbitTorrent/qbitTorrent')
          }
        },
        fail: e => {
          wx.showToast({
            title: "请求失败",
            icon: 'error',
            duration: 2000
          })
        }
      })
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    if (wx.getStorageSync('qbittoken')) {
      wx.navigateTo({
        url: '/pages/qbitTorrent/qbitTorrent',
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})