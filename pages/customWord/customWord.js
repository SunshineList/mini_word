// pages/customWord/customWord.js
var app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    checkboxArr: [{
      name: '已遗忘单词',
      checked: false
    }, {
      name: '不熟悉单词',
      checked: false
    }, {
      name: '常规单词',
      checked: true
    }],
    word: "",
    speech: "",
    type: "",
    mean: ""
  },

  getWordValue: function (e) {
    this.setData({
      word: e.detail.value
    })
  },

  getSpeechValue: function (e) {
    this.setData({
      speech: e.detail.value
    })
  },

  getMeanhValue: function (e) {
    this.setData({
      mean: e.detail.value
    })
  },

  radio: function (e) {
    var index = e.currentTarget.dataset.index;//获取当前点击的下标
    var checkboxArr = this.data.checkboxArr;//选项集合
    if (checkboxArr[index].checked) return;//如果点击的当前已选中则返回
    checkboxArr.forEach(item => {
      item.checked = false
    })
    checkboxArr[index].checked = true;//改变当前选中的checked值
    this.setData({
      checkboxArr: checkboxArr
    });
  },
  radioChange: function (e) {
    var checkValue = (parseInt(e.detail.value) + 1) + '';
    this.setData({
      type: checkValue
    });
  },

  wordSave: function () {
    var that = this
    if (!wx.getStorageInfoSync("token")) {
      wx.showToast({
        title: '请先登录',
        icon: "none",
        duration: 2000
      })
      wx.navigateTo({
        url: '/pages/login/login',
      })
    }
    wx.request({
      url: app.globalData.globalReqUrl + '/v1/api/word/custom_word/',
      data: { "word": that.data.word, "speech": that.data.speech, "type": that.data.type, "mean": that.data.mean },
      method: "POST",
      header: {
        'content-type': 'application/x-www-form-urlencoded', // 默认值
        'Authorization': 'Token ' + wx.getStorageSync("token")
      },
      success: res => {
        console.log(res.data)
        if (res.statusCode === 403) {
          wx.showToast({
            title: '请先登录',
            icon: "none",
            duration: 2000
          })
          wx.navigateTo({
            url: '/pages/login/login',
          })
        } else if (res.statusCode === 400) {
          wx.showToast({
            title: res.data.error,
            icon: "none",
            duration: 2000
          })
        } else if (res.statusCode === 201) {
          wx.showToast({
            title: '单词创建成功',
            icon: "success",
            duration: 2000
          })
          wx.navigateTo({
            url: '/pages/customWord/customWord',
          })
        }
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})