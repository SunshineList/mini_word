//获取应用实例
const app = getApp()

Page({
  data: {
    username: 'admin',
    password: '1234qwer'
  },
  //事件处理函数
  bindViewTap: function () {

  },
  onShow: function () {
    // 生命周期函数--监听页面显示
  },
  onLoad: function () {

  },


  // 获取输入账号 
  usernameInput: function (e) {
    this.setData({
      username: e.detail.value
    })
  },

  // 获取输入密码 
  passwordInput: function (e) {
    this.setData({
      password: e.detail.value
    })
  },

  // 登录处理
  login: function () {
    var that = this;
    if (this.data.username.length == 0 || this.data.password.length == 0) {
      wx.showToast({
        title: '账号或密码不能为空',
        icon: 'none',
        duration: 2000
      })
    } else {
      wx.request({
        url: app.globalData.globalReqUrl + '/v1/api/account/login/',
        method: 'post',
        data: {
          username: that.data.username,
          pwd: that.data.password
        },
        header: {
          'content-type': 'application/x-www-form-urlencoded' // 默认值
        },
        success: res => {
          if (res.statusCode == 200) {
            var token = res.data.token
            var createdTime = new Date()
            // 将token存入缓存
            wx.setStorageSync('token', token)
            wx.setStorageSync('createTime', createdTime)
            wx.setStorageSync('userInfo', res.data.user)
            wx.switchTab({
              url: '/pages/custom/custom',
            })
          } else {
            wx.showToast({
              title: res.data.error,
              icon: 'none',
              duration: 2000
            })
          }
        }
      })
    }
  }
})
