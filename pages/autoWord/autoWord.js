// pages/word/word.js
var app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    word: "",
    speech: "",
    mean: ""
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    that.netWord();
  },
  netWord: function () {
    wx.request({
      url: app.globalData.globalReqUrl + "/v1/api/word/custom_word/random_word/",
      header: { 'Authorization': 'Token ' + wx.getStorageSync("token") },
      method: "GET",
      success: res => {
        if (res.statusCode === 200) {
          this.setData({
            word: res.data.word,
            speech: res.data.speech,
            mean: res.data.mean
          })
        } else {
          wx.showToast({
            title: res.data.error,
            icon: "none",
            duration: 2000
          })
        }
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})