// pages/magent/magent.js
var app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    magent: ""
  },

  getInputValue(e) {
    console.log(e)
    this.setData({
      'magent': e.detail
    })
  },

  btnClick() {
    if (!this.data.magent) {
      wx.showToast({
        title: '种子链接地址不能为空',
        icon: 'error',
        duration: 3000
      })
    }else if (!(/^(magnet:\?xt=urn:btih:)[0-9a-fA-F]{40}.*$/).test(this.data.magent) && !(/(http|https):\/\/([\w.]+\/?)\S*/ig).test(this.data.magent)) {
      wx.showToast({
        title: '种子链接地址不规范',
        icon: 'error',
        duration: 3000
      })
    } else {
      wx.request({
        url: app.globalData.globaQbitUrl + "/download",
        method: 'POST',
        data: { 'magent': this.data.magent },
        success: res => {
          if (res.data.code != 200) {
            wx.showToast({
              title: res.data.message,
              icon: 'error',
              duration: 3000
            })
          } else {
            wx.showToast({
              title: "添加种子成功",
              icon: 'success',
              duration: 3000
            })
            setTimeout(() => {
              wx.navigateTo({
                url: '/pages/qbitTorrent/qbitTorrent',
              })
            }, 2000)
          }
        }
      })
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})