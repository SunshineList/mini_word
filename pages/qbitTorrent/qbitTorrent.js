// pages/qbitTorrent/qbitTorrent.js
var app = getApp()
Page({
  /**
   * 页面的初始数据
   */
  data: {
    torrentList: [],
    hasDownload: false,
    timer: null,
    hash: null,
    show: false,
    isStart: null,
    isPause: null,
    isOver: false
  },

  cellClick(e) {
    // if (this.data.isOver) {
    //   return
    // }
    this.setData({
      "hash": e.target.id,
    })
    this.showPopup()
  },

  start() {
    wx.request({
      url: app.globalData.globaQbitUrl + '/start',
      method: "POST",
      data: { 'hash': this.data.hash },
      success: res => {
        if (res.statusCode != 200) {
          wx.showToast({
            title: res.data.message,
            icon: 'error'
          })
        } else {
          wx.showToast({
            title: "恢复任务下载",
            icon: 'success',
            duration: 2000
          })
          setTimeout(()=>{
            wx.navigateTo({
              url: '/pages/qbitTorrent/qbitTorrent',
            })
          }, 2000)
          this.setData({
            'show': false
          })
        }
      }
    })
  },


  btnClick(){
    wx.navigateTo({
      url: '/pages/magent/magent',
    })
  },

  pause() {
    wx.request({
      url: app.globalData.globaQbitUrl + '/pause',
      method: "POST",
      data: { 'hash': this.data.hash },
      success: res => {
        if (res.statusCode != 200) {
          wx.showToast({
            title: res.data.message,
            icon: 'error'
          })
        } else {
          wx.showToast({
            title: "暂停任务下载",
            icon: 'success',
            duration: 2000
          }),
          setTimeout(()=>{
            this.onLoad()
          }, 2000)
          this.setData({
            'show': false
          })
        }
      }
    })
  },

  showPopup() {
    this.setData({ show: true });
  },

  onClose() {
    this.setData({ show: false });
  },

  api_request() {
    if (!this.data.hasDownload) {
      clearInterval(this.data.timer)
      return
    }
    wx.request({
      url: app.globalData.globaQbitUrl + '/torrent_list',
      method: 'GET',
      success: res => {
        if (res.data.code != 200) {
          wx.setStorageSync('qbittoken', null)
          wx.switchTab({
            url: '/pages/qbit/qbit',
          })
        } else {
          this.setData({
            'torrentList': res.data.data.data,
            'hasDownload': res.data.data.has_download,
          })
        }
      },
      fail: e => {
        wx.setStorageSync('qbittoken', null)
        wx.switchTab({
          url: '/pages/qbit/qbit',
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.setData({
      'hasDownload': true
    })
    this.data.timer = setInterval(() => this.api_request(), 2000)
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    this.setData({
      'hasDownload': false
    })
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})