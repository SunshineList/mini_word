App({
  // =======全局数据对象（可以整个应用程序共享）======
  globalData: {
    globalReqUrl: "https://wangzw.outputbug.com",
    globaQbitUrl: "https://wangzw.outputbug.com/qbit",
    userInfo: wx.getStorageSync('userInfo')
  },

  checkLogin: function () {
    var time = new Date();
    var createTime = wx.getStorageSync('createTime');
    var token = wx.getStorageSync('token');
    if (!token) {
      wx.showToast({
        title: '请先登录',
        icon: 'none',
        duration: 2000
      })
      //不存在token，调用登录
      this.globalData.userInfo = {}
      wx.navigateTo({
        url: '/pages/login/login',
      })
    } else if (time - createTime > 6 * 24 * 3600 * 1000) {
      console.log('token expired')
      this.globalData.userInfo = {}
      wx.showToast({
        title: '请先登录',
        icon: 'none',
        duration: 2000
      })
      wx.navigateTo({
        url: '/pages/login/login',
      })
    } else {
      console.log('already Login')
    }
  },

  jumpToPage: function (pagePath) {
    if (!wx.getStorageSync("token")) {
      wx.showToast({
        title: '请先登录',
        icon: "none",
        duration: 2000
      })
      wx.navigateTo({
        url: '/pages/login/login',
      })
    } else {
      wx.navigateTo({
        url: pagePath,
      })
    }
  },

  checkQbitLogin: function () {
    var token = wx.getStorageSync('qbittoken');
    if (!token) {
      wx.showToast({
        title: '请先登录',
        icon: 'none',
        duration: 2000
      })
      wx.navigateTo({
        url: '/pages/qbit/qbit',
      })
    } else {
      console.log('already Login')
    }
  },

  jumpToQbitPage: function (pagePath) {
    if (!wx.getStorageSync("qbittoken")) {
      wx.showToast({
        title: '请先登录',
        icon: "none",
        duration: 2000
      })
      wx.navigateTo({
        url:'/pages/qbit/qbit',
      })
    } else {
      wx.navigateTo({
        url: pagePath,
      })
    }
  },

  //应用程序启动时触发一次
  onLaunch() {
    // this.checkLogin()
  },

  //当应用程序进入前台显示状态时触发
  onShow() { },

  //当应用程序进入后台状态时触发
  onHide() { }

})